﻿var NRS = (function (NRS, $, undefined) {
    var isDebug = false;
    var initNewbieURL = " http://jnxt.org";
    var mgwServer = ["209.126.70.170", "209.126.70.156", "209.126.70.159"];

    var _bridge = [
    { "coin": "BTCD", "bridge": "http://178.63.60.131" },
    { "coin": "BITS", "bridge": "http://178.63.60.131" },
    { "coin": "OPAL", "bridge": "http://178.63.60.131" },
    { "coin": "VRC", "bridge": "http://178.63.60.131" },
    { "coin": "VPN", "bridge": "http://178.63.60.131" }
    ];

    var _coin = [
    { "coin": "BTC", "assetID": "17554243582654188572", "decimal": 8, "depositConfirmation": "6", "balance": 0 },
    { "coin": "BTCD", "assetID": "6918149200730574743", "decimal": 4, "depositConfirmation": "10", "balance": 0 },
    { "coin": "VRC", "assetID": "9037144112883608562", "decimal": 8, "depositConfirmation": "3", "balance": 0 },
    { "coin": "OPAL", "assetID": "6775076774325697454", "decimal": 8, "depositConfirmation": "3", "balance": 0 },
    { "coin": "BITS", "assetID": "13120372057981370228", "decimal": 6, "depositConfirmation": "3", "balance": 0 },
    { "coin": "VPN", "assetID": "7734432159113182240", "decimal": 4, "depositConfirmation": "3", "balance": 0 }
    ];

    var _coinMGW = [
    { "coin": "BTC", "accountRS": "NXT-3TKA-UH62-478B-DQU6K" },
    { "coin": "BTCD", "accountRS": "NXT-8RQH-HFUP-3AJ9-E2DB9" },
    { "coin": "VRC", "accountRS": "NXT-8RQH-HFUP-3AJ9-E2DB9" },
    { "coin": "OPAL", "accountRS": "NXT-8RQH-HFUP-3AJ9-E2DB9" },
    { "coin": "BITS", "accountRS": "NXT-8RQH-HFUP-3AJ9-E2DB9" },
    { "coin": "VPN", "accountRS": "NXT-8RQH-HFUP-3AJ9-E2DB9" }
    ];

    var _coinSer = [
    { "coin": "BTC", "server": ["8593269027165738667", "2986384496629142530", "2406158154854548637", "12736719038753962716"] },
    { "coin": "BTCD", "server": ["14124705753332172426", "17920794290292874339", "9137544650187003569", "14097780291918847695"] },
    { "coin": "VRC", "server": ["14124705753332172426", "17920794290292874339", "9137544650187003569", "14097780291918847695"] },
    { "coin": "OPAL", "server": ["14124705753332172426", "17920794290292874339", "9137544650187003569", "14097780291918847695"] },
    { "coin": "BITS", "server": ["14124705753332172426", "17920794290292874339", "9137544650187003569", "14097780291918847695"] },
    { "coin": "VPN", "server": ["14124705753332172426", "17920794290292874339", "9137544650187003569", "14097780291918847695"] }
    ];

    $(document).ready(function () {
        getServerStatus();
        initActiveCoin();
    });

    NRS.spnInit = function () {
        var isNewbieAcc = false;
        
        $("#lockscreen").hide();
        //$("#spn_newbie").show();
        
        showNewbieContent(false);
        $(".bgnxt h4").text(NRS.accountRS);
    }

    NRS.spnNewBlock = function () {
        getBalance();
        getServerStatus();
    }

    $('#spn_newbie_submit').on('click', function () {
        var $btn = $(this).button('loading');

        sendNewbieInitRequest(1);
    });

    function initActiveCoin() {
        $.each(_bridge, function (i, v) {
            $(".bg" + v.coin.toLowerCase() + " h4").html('In Queue');
        });
    }

    function addDashboardCSS() {
        includeCSSfile("spn/css/reset.css");
        includeCSSfile("spn/css/style.css");
    }

    function includeCSSfile(href) {
        var head_node = document.getElementsByTagName('head')[0];
        var link_tag = document.createElement('link');
        link_tag.setAttribute('rel', 'stylesheet');
        link_tag.setAttribute('type', 'text/css');
        link_tag.setAttribute('href', href);
        head_node.appendChild(link_tag);
    }

    function showNewbieContent(isNewbie) {
        if (isNewbie) {
            $("#spn_newbie .loading").hide();
            $("#spn_newbie .content").show();
        }
        else {
            //$("#spn_newbie .loading").show();
            showDashboard();

            if (_bridge.length > 0) {
                sendNewbieInitRequest(0, 1);
            }
        }
    }
    
    function showDashboard() {
        addDashboardCSS();
        getBalance();

        setTimeout(function () {
            $("#spn_dashboard").show();
            NRS.unlock();
        }, 500);
    }

    function getBalance() {
        NRS.sendRequest("getAccountAssets", {
            account: NRS.accountRS
        }, function (response) {
            if (response.accountAssets) {
                $.each(response.accountAssets, function (i, v) {
                    var coinDetails = $.grep(_coin, function (coinD) { return coinD.assetID == v.asset });

                    if (coinDetails.length > 0) {
                        var balance = NRS.convertToNXT(new BigInteger(v.unconfirmedQuantityQNT).multiply(new BigInteger(Math.pow(10, 8 - v.decimals).toString())));
                        coinDetails[0].balance = balance;
                    }
                });
            }

            $.each(_coin, function (i, v) {
                $(".bg" + v.coin.toLowerCase() + " h5").text(v.balance);
            });
        });
    }

    function sendNewbieInitRequest(index, tries) {
        var url = _bridge[index].bridge + "/init/?requestType=newbie";
        url += "&coin=" + _bridge[index].coin;
        url += "&NXT=" + NRS.accountRS;
        url += "&pubkey=" + NRS.publicKey;
        url += "&convertNXT=10";

        if ($("#newbie_email").val().trim()) {
            url += "&email=" + $("#newbie_email").val().trim();
        }

        if ($("#newbie_nxt").val().trim()) {
            //url += "&convertNXT=" + $("#newbie_nxt").val().trim();
        }

        //$.growl(url, { "type": "success" });

        sendNewbieAjaxRequest(url, tries, index);
    }

    function sendNewbieAjaxRequest(url, tries, index) {
        var coin = url.split('&')[1].split('=')[1];

        if (tries > 3) {
            $(".bg" + coin.toLowerCase() + " h4").html('Error getting deposit address');

            if (index + 1 < _bridge.length) {
                sendNewbieInitRequest(++index, 1)
            }
            return;
        }
        //$.growl("Trying to get deposit address tries : " + tries, { "type": "success" });
        
        //dev
        //showDepositAddr(JSON.parse('[[{"requestType":"MGWaddr","sender":"423766016895692955","buyNXT":0,"created":1419586879,"M":2,"N":3,"NXTaddr":"3705364957971254799","RS":"NXT-X5JH-TJKJ-DVGC-5T2V8","address":"bThdNGHxZEe7LUD5waaSEWreAELsrtNjVp","redeemScript":"5221027d7ca4916cde17088707f2db07530f4b0f6e1191444e8a34719a384185b2b37f2103395682fb6fabda7e76b9a05cbd8386bc0c9175503d6be259bd4fcc57b32d535d2102ebfaf9ff68cc6147c200b336d9ef3c5c0cba60033abf393d09015215a9f53af253ae","coin":"BTCD","gatewayid":"0","pubkey":[{"address":"RCToyGor3Aq6n7gsZKRT4XiDEWFy4d4n9s","pubkey":"027d7ca4916cde17088707f2db07530f4b0f6e1191444e8a34719a384185b2b37f","srv":"423766016895692955"},{"address":"RCnoTsLsZvhfAUPwNVPSx3nxAQXHSR51Cr","pubkey":"03395682fb6fabda7e76b9a05cbd8386bc0c9175503d6be259bd4fcc57b32d535d","srv":"12240549928875772593"},{"address":"RMU6LLrrE7CFFG7BWfjaycNnPzJtunJ7R3","pubkey":"02ebfaf9ff68cc6147c200b336d9ef3c5c0cba60033abf393d09015215a9f53af2","srv":"8279528579993996036"}]},{"token":"4nbbkmqel5nl6fevock6k7gbpcfmt9fl3gsj4gf3e7rdefsd1scqo6ebidq37jg2bk1uu3jb0ontgc0lb2s3jhksg8f8bakogt02t8of52rgbbamvj6dk0d9bb3rvto7fjcqb7c2jfon3n11chbtophrjrma1i6q"}],[{"requestType":"MGWaddr","sender":"12240549928875772593","buyNXT":0,"created":1419586879,"M":2,"N":3,"NXTaddr":"3705364957971254799","RS":"NXT-X5JH-TJKJ-DVGC-5T2V8","address":"bThdNGHxZEe7LUD5waaSEWreAELsrtNjVp","redeemScript":"5221027d7ca4916cde17088707f2db07530f4b0f6e1191444e8a34719a384185b2b37f2103395682fb6fabda7e76b9a05cbd8386bc0c9175503d6be259bd4fcc57b32d535d2102ebfaf9ff68cc6147c200b336d9ef3c5c0cba60033abf393d09015215a9f53af253ae","coin":"BTCD","gatewayid":"1","pubkey":[{"address":"RCToyGor3Aq6n7gsZKRT4XiDEWFy4d4n9s","pubkey":"027d7ca4916cde17088707f2db07530f4b0f6e1191444e8a34719a384185b2b37f","srv":"423766016895692955"},{"address":"RCnoTsLsZvhfAUPwNVPSx3nxAQXHSR51Cr","pubkey":"03395682fb6fabda7e76b9a05cbd8386bc0c9175503d6be259bd4fcc57b32d535d","srv":"12240549928875772593"},{"address":"RMU6LLrrE7CFFG7BWfjaycNnPzJtunJ7R3","pubkey":"02ebfaf9ff68cc6147c200b336d9ef3c5c0cba60033abf393d09015215a9f53af2","srv":"8279528579993996036"}]},{"token":"phfmcvvc65ag4edesl43pith02t6r4c4c3e4s0t7gofh3p7p1scrin8k932ieug2n71lit3h6mtm0ff8l6m34lhq0v0icensk21n7irhr9lgavdvvtfqe2d917g303fgb958n220b406vvq58cu095d2a46upra2"}],[{"requestType":"MGWaddr","sender":"8279528579993996036","buyNXT":0,"created":1419586879,"M":2,"N":3,"NXTaddr":"3705364957971254799","RS":"NXT-X5JH-TJKJ-DVGC-5T2V8","address":"bThdNGHxZEe7LUD5waaSEWreAELsrtNjVp","redeemScript":"5221027d7ca4916cde17088707f2db07530f4b0f6e1191444e8a34719a384185b2b37f2103395682fb6fabda7e76b9a05cbd8386bc0c9175503d6be259bd4fcc57b32d535d2102ebfaf9ff68cc6147c200b336d9ef3c5c0cba60033abf393d09015215a9f53af253ae","coin":"BTCD","gatewayid":"2","pubkey":[{"address":"RCToyGor3Aq6n7gsZKRT4XiDEWFy4d4n9s","pubkey":"027d7ca4916cde17088707f2db07530f4b0f6e1191444e8a34719a384185b2b37f","srv":"423766016895692955"},{"address":"RCnoTsLsZvhfAUPwNVPSx3nxAQXHSR51Cr","pubkey":"03395682fb6fabda7e76b9a05cbd8386bc0c9175503d6be259bd4fcc57b32d535d","srv":"12240549928875772593"},{"address":"RMU6LLrrE7CFFG7BWfjaycNnPzJtunJ7R3","pubkey":"02ebfaf9ff68cc6147c200b336d9ef3c5c0cba60033abf393d09015215a9f53af2","srv":"8279528579993996036"}]},{"token":"u9pvu95ch7jei98kc641u51ms5p6u85cghl8lkefn4f22v3k1scqk0401fe7fcg2i9tt6bnip71dvtnkck9rfu393ia968ibgpjjfirlj8rgvgcl2sv5duprbj6ee2f54865kri8bslhv6vn165464drj7fv757f"}]]'),"BTCD");
		//return;
        //end dev

        $(".bg" + coin.toLowerCase() + " h4").html('Generating deposit address <span class="loading_dots"><span>.</span><span>.</span><span>.</span></span>');

        $.ajax({
            url: url,
            dataType: 'text',
            type: 'GET',
            timeout:30000,
            crossDomain: true,
            success: function (data) {
                if (!IsJsonString(data)) {
                    data = removeWarningJsonReturn(data);
                }
                data = JSON.parse(data);

                var bIsValid = false;

                if (isDebug) {
                    $.growl("data return : " + JSON.stringify(data), { "type": "success" });
                }
                
                if (data[0]) {
                    if (data[0][0].RS) {
                        if (data[0][0].RS == NRS.accountRS) {
                            bIsValid = true;
                        }
                    }
                }
                
                if (bIsValid) {
                    showDepositAddr(data, coin, index);
                }
                else {
                    setTimeout(function () { sendNewbieInitRequest(url, ++tries, index); }, 5000);
                }
            },
            error: function (x, t, m) {
                if (isDebug) {
                    $.growl("Error calling " + url + " API", { "type": "danger" });
                }
                setTimeout(function () { sendNewbieInitRequest(index, ++tries); }, 5000);
            }
        });
    }

    //function sendMGWmsigStatus(tries) {
    //    $.growl("sending MGW msig API...", { "type": "success" });

    //    $.each(mgwServer, function (i, v) {
    //        var url = "http://" + v + "/MGW/msig/" + NRS.account;

    //        $.ajax({
    //            url: url,
    //            dataType: 'json',
    //            type: 'GET',
    //            timeout: 30000,
    //            crossDomain: true,
    //            success: function (data) {
    //                $.growl("mgw server data return : " + JSON.stringify(data), { "type": "success" });
    //                var bIsValid = false;

    //                if (data[0]) {
    //                    if (data[0].RS) {
    //                        if (data[0].RS == NRS.accountRS) {
    //                            bIsValid = true;
    //                        }
    //                    }
    //                }

    //                if (bIsValid) {
    //                    showDepositAddrFromMGWmsig(data);
    //                }
    //            },
    //            error: function (x, t, m) {
    //                $.growl("Error calling mgwServer " + v + " API", { "type": "danger" });

    //                if (i == 0) {
    //                    setTimeout(function () { sendNewbieInitRequest(++tries); }, 10000);
    //                }
    //            }
    //        });
    //    });
    //}

    function showDepositAddr(data, coin, index) {
        var coinAddr = "";

        $.each(data, function (i, v) {
            $(".bg" + data[i][0].coin.toLowerCase() + " h4").text(data[i][0].address);
        });

        if (index + 1 < _bridge.length) {
            sendNewbieInitRequest(++index, 1)
        }
    }

    function showDepositAddrFromMGWmsig(data) {
        if (data[0].coin == "BTCD") {
            $(".bgbtcd h4").text(data[0].address);
        }
        showDashboard();
    }

    function sentNXT() {
        NRS.sendRequest("sendMoney", {
            secretPhrase: "",
            feeNQT: "100000000",
            deadline: "1440",
            recipient: $("#field114cont").val(),
            amountNXT: $("#field113cont").val()
        }, function (response) {
            if (response.errorCode) {
                $("#field115cont").val(response.errorDescription);
            }

            if (response.transaction) {
                $("#field115cont").val("TX id :" + response.transaction);
            } else {
                $("#field115cont").val("Unexpected Error");
            }
        });
    }

    function sentMGWcoin(coin) {
        var message = '{"redeem":"' + coin + '","withdrawaddr":"' + $("#field114cont").val() + '","InstantDEX":""}';
        var coinDetails = $.grep(_coin, function (coinD) { return coinD.coin == coin });
        var coinMGW = $.grep(_coinMGW, function (coinD) { return coinD.coin == coin });
        
        if (isWithdrawValid(coinDetails[0].assetID, coinDetails[0].coin, coinDetails[0].decimal)) {
            $("#field115cont").val("");
            NRS.sendRequest("transferAsset", {
                secretPhrase: "",
                message: message,
                feeNQT: "100000000",
                deadline: "1440",
                recipient: coinMGW[0].accountRS,
                asset: coinDetails[0].assetID,
                quantityQNT: NRS.convertToQNT($("#field113cont").val(), coinDetails[0].decimal),
                merchant_info: ""
            }, function (response) {
                if (response.errorCode) {
                    $("#field115cont").val(response.errorDescription);
                }

                if (response.transaction) {
                    $("#field115cont").val("TX id :" + response.transaction);

                    setTimeout(function () {
                        getBalance();
                    }, 5000);
                } else {
                    $("#field115cont").val("Unexpected Error");
                }
            });
        }
    }

    function isWithdrawValid(coinID,coin,decimal) {
        var result = true;
        var balanceNQT = 0;
        
        if (NRS.accountInfo.unconfirmedAssetBalances) {
            for (var i = 0; i < NRS.accountInfo.unconfirmedAssetBalances.length; i++) {
                var balance = NRS.accountInfo.unconfirmedAssetBalances[i];

                if (balance.asset == coinID) {
                    balanceNQT = balance.unconfirmedBalanceQNT.toString();
                }
            }
        }

        if ($.trim($("#field113cont").val()) == "") {
            $("#field115cont").val("Please specify your amount");
            return false;
        }

        if ($.trim($("#field114cont").val()) == "") {
            $("#field115cont").val("Please specify your " + coin + " address");
            return false;
        }

        balanceNQT = new Big(balanceNQT.toString());
        var withdrawAmountNQT = new Big(NRS.convertToQNT($("#field113cont").val(), decimal).toString());

        if (balanceNQT.cmp(withdrawAmountNQT) == -1) {
            $("#field115cont").val("You have insufficient " + coin + " to withdraw");
            return false;
        }

        if (NRS.accountInfo.unconfirmedBalanceNQT < 100000000) {
            $("#field115cont").val("Withdraw " + coin + " required 1 NXT");
            return false;
        }
        return result;
    }

    function getMGWaddr() {
        var resultTXID = [];

        NRS.sendRequest("getUnconfirmedTransactions", {
            account: NRS.accountRS            
        }, function (response) {
            if (response.unconfirmedTransactions.length > 0) {
                $.each(response.unconfirmedTransactions, function (i, v) {
                    if (v.type == 1 && v.subtype == 0) {
                        //Arbitrary message
                        
                        var msg = JSON.parse(v.attachment.message);
                        var coinSer = $.grep(_coinSer, function (coinSer) { return coinSer.coin == msg.coin });

                        for (var x = 0; x < coinSer[0].server.length; x++) {
                            if (v.sender == coinSer[0].server[x]) {
                                var addr = v.address;

                                if (isDebug) {
                                    $.growl("mgw addr for " + msg.coin + " : " + addr, { "type": "success" });
                                }
                            }
                        }
                    }
                });
            }
            else {
                //TODO
                //check AM
            }
        });
    }

    //NRS.getTxHistory = function() {
    //    $("#tx_history_modal_transactions").addClass("data-loading");

    //    var hasResult = false;
    //    var url = initNewbieURL + "/init/?requestType=status";
    //    url += "&NXT=" + NRS.accountRS;
    //    url += "&pubkey=" + NRS.publicKey;
    //    url += "&coin=BTCD";

    //    $("#txt_tx_API").val(url);

    //    $.ajax({
    //        url: url,
    //        dataType: 'json',
    //        type: 'GET',
    //        timeout: 30000,
    //        crossDomain: true,
    //        success: function (data) {
    //            $.growl("tx history data return : " + JSON.stringify(data), { "type": "success" });
                
    //            $("#txt_tx_log").val(JSON.stringify(data));
    //            //var result = JSON.parse('[[{"circulation":"8104.81340000","unspent":"8094.80344007","pendingdeposits":"0.00000000","redeems":[],"revenues":"-10.00995993","boughtNXT":30,"seconds":"29.614","userdeposits":[{"vout":1,"height":299085,"txid":"9f22aaa6da9afe9e8fcf1c8a4d69573f9a7264a529cbb71c8bf8ef8de824e810","addr":"bLRp614oYpZCvJ3PjJpJG9Kw4GQBh9ui84","value":"0.20000000","depositid":"15017275505506004531","status":"complete"}],"userwithdraws":[],"AEbalance":"0.20000000","pending_userdeposits":"0.00000000","pending_userwithdraws":"0.00000000","userbalance":"0.20000000","userNXT":"8026315745545145508","userRS":"NXT-HM76-NNU7-LL7S-8MTSY","depositaddr":"bLRp614oYpZCvJ3PjJpJG9Kw4GQBh9ui84","NXT":"423766016895692955","requestType":"MGWresponse","gatewayid":0,"timestamp":1419828158,"NXTheight":319956,"depinfo":[-10.00995993,-0.00995993,0.09004007,8094.80344007,8104.80344007,8104.90344007,0,0.20000000,0.20000000,0,0,0,0,0,0]},{"token":"4nbbkmqel5nl6fevock6k7gbpcfmt9fl3gsj4gf3e7rdefsd1roo26ebankapoo2bs95gkhu5lqqrm2tkqdclaht356b2165oesjq23d8kc0ft4o41j2gn2bkk7cq5ss29v6hlkvq2kh4e993guup8qgfra94f6u"}],[{"circulation":"8104.81340000","unspent":"8104.80344007","pendingdeposits":"0.00000000","redeems":[],"revenues":"-0.00995993","boughtNXT":30,"seconds":"5.180","userdeposits":[{"vout":1,"height":299085,"txid":"9f22aaa6da9afe9e8fcf1c8a4d69573f9a7264a529cbb71c8bf8ef8de824e810","addr":"bLRp614oYpZCvJ3PjJpJG9Kw4GQBh9ui84","value":"0.20000000","depositid":"15017275505506004531","status":"complete"}],"userwithdraws":[],"AEbalance":"0.20000000","pending_userdeposits":"0.00000000","pending_userwithdraws":"0.00000000","userbalance":"0.20000000","userNXT":"8026315745545145508","userRS":"NXT-HM76-NNU7-LL7S-8MTSY","depositaddr":"bLRp614oYpZCvJ3PjJpJG9Kw4GQBh9ui84","NXT":"12240549928875772593","requestType":"MGWresponse","gatewayid":1,"timestamp":1419828206,"NXTheight":319956,"depinfo":[-10.00995993,-0.00995993,0.09004007,8094.80344007,8104.80344007,8104.90344007,0,0,0.20000000,0,0,0,0,0,0]},{"token":"phfmcvvc65ag4edesl43pith02t6r4c4c3e4s0t7gofh3p7p1roqun8klkn6p382br97nl5sb66tmugggb5rs84feer4qucbhnvfb5f67gpgusrhv6ch98s1cnl5irilin2fhqpt1eores0c1nbjuiih98ialenu"}]]');
    //            var result = data;
    //            var rows = "";

    //            if (result[0]) {
    //                if (result[0][0]) {
    //                    if (result[0][0].userdeposits) {
    //                        hasResult = true;
    //                    }
    //                }
    //            }

    //            if (hasResult) {
    //                $.each(result[0][0].userdeposits, function (i, v) {
    //                    rows += "<tr><td>" + v.value + "</td><td>" + v.status + "</td></tr>";
    //                });

    //                $("#tx_history_modal_transactions_table tbody").empty().append(rows);
    //            }
    //            else {
    //                $("#tx_history_modal_transactions_table tbody").empty();
    //            }

    //            setTimeout(function () {
    //                NRS.dataLoadFinished($("#tx_history_modal_transactions_table"));
    //            }, 1000);
    //        },
    //        error: function (x, t, m) {
    //            $.growl("Error calling " + url + " API", { "type": "danger" });
    //        }
    //    });
    //}
    
    function getTxHistory(coin) {
        $("#tx_deposit_table").parent().addClass("data-loading").removeClass("data-empty");
        $("#tx_withdraw_table").parent().addClass("data-loading").removeClass("data-empty");

        var coinDetails = $.grep(_coin, function (coinD) { return coinD.coin == coin });

        if (coin == "NXT") {
            $("#column_debit").text("From");
            $("#column_credit").text("To");
            NRS.sendRequest("getAccountTransactions", {
                account: NRS.accountRS,
                type: "0",
                subtype: "0"
            }, function (response) {
                if (response.errorCode) {
                    //empty history
                    NRS.dataLoadFinished($("#tx_deposit_table"));
                    NRS.dataLoadFinished($("#tx_withdraw_table"));
                    return;
                }

                if (response.transactions) {
                    var deposit = "";
                    var withdraw = "";

                    $.each(response.transactions, function (i, v) {
                        if (v.recipientRS == NRS.accountRS) {
                            deposit += "<tr><td>" + NRS.formatTimestamp(v.timestamp) + "</td><td>" + v.senderRS + "</td><td>" + NRS.convertToNXT(v.amountNQT) + "</td></tr>";
                        }
                        //withdraw
                        if (v.senderRS == NRS.accountRS) {
                            withdraw += "<tr><td>" + NRS.formatTimestamp(v.timestamp) + "</td><td>" + v.recipientRS + "</td><td>" + NRS.convertToNXT(v.amountNQT) + "</td></tr>";
                        }
                    });

                    setTimeout(function () {
                        $("#tx_deposit_table tbody").empty().append(deposit);
                        $("#tx_withdraw_table tbody").empty().append(withdraw);

                        NRS.dataLoadFinished($("#tx_deposit_table"));
                        NRS.dataLoadFinished($("#tx_withdraw_table"));
                    }, 1000);
                }
            });
        } else {
            $("#column_debit").text("Debit");
            $("#column_credit").text("Credit");
            NRS.sendRequest("getAccountTransactions", {
                account: NRS.accountRS,
                type: "2",
                subtype: "1"
            }, function (response) {
                if (response.errorCode) {
                    //empty history
                    NRS.dataLoadFinished($("#tx_deposit_table"));
                    NRS.dataLoadFinished($("#tx_withdraw_table"));
                    return;
                }

                if (response.transactions) {
                    var deposit = "";
                    var withdraw = "";

                    $.each(response.transactions, function (i, v) {
                        if (isValidMgwServer(coin, v.sender)) {
                            if (v.attachment.asset == coinDetails[0].assetID) {
                                //deposit += v.attachment.message;
                                //$.growl(v.attachment.message, { "type": "danger" });
                                var msg = JSON.parse(v.attachment.message);
                                var nxtAdded = "";

                                if (msg.buyNXT && msg.conv != 0) {
                                    nxtAdded = "+ " + msg.buyNXT + " NXT";
                                }

                                deposit += "<tr><td>" + NRS.formatTimestamp(v.timestamp) + "</td><td>" + NRS.formatAmount(msg.amount) + "</td><td>" + NRS.formatQuantity(v.attachment.quantityQNT, coinDetails[0].decimal) + " " + coin + " " + nxtAdded + "</td></tr>";
                            }
                        }

                        //withdraw
                        if (isValidMgwServer(coin, v.recipient)) {
                            if (v.attachment.asset == coinDetails[0].assetID) {
                                //var msg = JSON.parse(v.attachment.message);

                                withdraw += "<tr><td>" + NRS.formatTimestamp(v.timestamp) + "</td><td>" + NRS.formatQuantity(v.attachment.quantityQNT, coinDetails[0].decimal) + "</td><td>" + "TODO" + "</td></tr>";
                            }
                        }
                    });

                    setTimeout(function () {
                        $("#tx_deposit_table tbody").empty().append(deposit);
                        $("#tx_withdraw_table tbody").empty().append(withdraw);

                        NRS.dataLoadFinished($("#tx_deposit_table"));
                        NRS.dataLoadFinished($("#tx_withdraw_table"));
                    }, 1000);
                }
            });
        }
    }

    function isValidMgwServer(coin, account) {
        var result = false;
        var coinSer = $.grep(_coinSer, function (coinD) { return coinD.coin == coin });

        for (var x = 0; x < coinSer[0].server.length; x++) {
            if (account == coinSer[0].server[x]) {
                result = true;
                return result;
            }
        }
        return result;
    }

    function getServerStatus() {
        $("#server_status_table tbody").empty();

        $.each(_bridge, function (i, v) {
            var url = v.bridge + "/init/?requestType=status&coin=" + v.coin + "&jsonflag=1";
            var rows = "", status = "", light = "";

            $.ajax({
                url: url,
                dataType: 'json',
                type: 'GET',
                timeout: 10000,
                crossDomain: true,
                success: function (data) {
                    NRS.sendRequest("getBlockchainStatus", {
                    }, function (response) {
                        if (response.lastBlockchainFeederHeight) {
                            $.each(data, function (i, v) {
                                var server_details = "Nxt Height : <nxt.height> <br/>Nxt Lag : <nxt.lag> <br/>Coin Height : <coin.height> <br/>Coin Lag : <coin.lag>";
                                var color = "red";
                                color = getServerStatusColor(v.RTNXT.height, v.RTNXT.lag, v[v.coin].height, v[v.coin].lag, response.lastBlockchainFeederHeight);

                                server_details = server_details.replace("<nxt.height>", v.RTNXT.height);
                                server_details = server_details.replace("<nxt.lag>", v.RTNXT.lag);
                                server_details = server_details.replace("<coin.height>", v[v.coin].height);
                                server_details = server_details.replace("<coin.lag>", v[v.coin].lag);

                                light += " <i class='fa fa-circle 1' style='font-size: 12px; color: " + color + "'></i>";
                                status += "<td>" + server_details + "</td>";
                            });


                            rows += "<tr><td>" + v.coin + light + "</td>";
                            rows += status;
                            rows += "</tr>";

                            $("#server_status_table tbody").append(rows);
                            NRS.dataLoadFinished($("#server_status_table"));
                        } else {
                            $("#server_status_table tbody").append(rows);
                            NRS.dataLoadFinished($("#server_status_table"));
                        }
                    });
                },
                error: function (x, t, m) {
                    if (isDebug) {
                        $.growl("Error getting " + v.coin + " server status!", { "type": "danger" });
                    }
                }
            });
        });
    }

    function getServerStatusColor(nxtHeight, nxtLag, coinHeight, coinLag,nxtLastBlockHeight) {
        var blockDiff = 10;
        var lagDiff  = 10;
        var multiply = 5;
        if (nxtLastBlockHeight - parseInt(nxtHeight) > 0 && nxtLastBlockHeight - parseInt(nxtHeight) > blockDiff) {
            if (nxtLastBlockHeight - parseInt(nxtHeight) > (blockDiff * multiply)) {
                return "red";
            } else {
                return "yellow";
            }
        }

        if (nxtLag > lagDiff) {
            if (nxtLag > (lagDiff * multiply)) {
                return "red";
            } else {
                return "yellow";
            }
        }

        if (coinLag > lagDiff) {
            if (coinLag > (lagDiff * multiply)) {
                return "red";
            }
            else {
                return "yellow";
            }
        }

        return "green";
    }

    function IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }

    function removeWarningJsonReturn(data) {
        return data.substr(data.indexOf("["));
    }

    $('#tx_history_modal').on('show.bs.modal', function (e) {
        NRS.getTxHistory();
    });

    $('#mgw_withdraw_modal').on('show.bs.modal', function (e) {
        $("#mgw_withdraw_modal_add_message").prop('checked', true);
        $("#mgw_withdraw_modal_recipient").val("NXT-JXRD-GKMR-WD9Y-83CK7");
        $("#mgw_withdraw_modal_asset").val("11060861818140490423");
        $("#mgw_withdraw_modal_feeNXT").val(1);
        $("#mgw_withdraw_modal_deadline").val(24);
    });

    $('#mgw_withdraw_modal').on('hidden.bs.modal', function () {
        setTimeout(function () {
            getBalance();
        }, 5000);
    })

    $('#tx_history_refresh').on('click', function () {
        NRS.getTxHistory();
    });
    
    $('#coinops_submit').on('click', function () {
        var coin = $('#modal-11 .md-head').attr("src").split('/').pop().split('_')[2];
        var option = $('#field111cont').val();

        switch (option) {
            case "3":
                //send coin
                if (coin == "nxt") {
                    sentNXT();
                }
                else if (coin == "btc" || coin == "btcd" || coin == "vrc" || coin == "opal" || coin == "bits" || coin == "vpn") {
                    sentMGWcoin(coin.toUpperCase());
                }
                else {
                    $("#field115cont").val("This operation is not implemented yet!");
                }
                break;
            default:
                $("#field115cont").val("This operation is not implemented yet!");
                break;
        }
    });
    
    $('#field111cont').on('change', function () {
        var option = $('#field111cont').val();

        switch (option) {
            case "3":
                //send coin
                //TODO hide unnecessary field 
                break;
            default:
                break;
        }
    });

    $('#getMGWaddr').on('click', function () {
        getMGWaddr();
    });

    $('#tab_tx_history li a').on('click', function (e) {
        getTxHistory($("#modal-tx-history h3").text().split(' ')[0]);
    });
    
    $('.cboxcont h5').on('click', function (e) {
        var coin = "";
        $.each($(this).parents('.cbox').attr('class').split(' '), function (i, v) {
            if (v.match("bg")) {
                coin = v.substr(2).toUpperCase();
            }
        });

        $("#modal-tx-history h3").html(coin + " Transaction History");
        getTxHistory(coin);
    });
    
    return NRS;
}(NRS || {}, jQuery));